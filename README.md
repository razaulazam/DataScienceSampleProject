# Data Science Sample Project
This project consists of a Jupyter notebook and a PDF file* named as 'analysis_jupyter_file' comprising of the code, written using Python's Pandas and Scikit-learn library, which shows the steps that are required for analyzing the data and accessing whether we could develop predictive Machine learning models for some certain target variables.

*It is recommended to open the PDF file instead of the Jupyter notebook since its size is pretty huge. 
